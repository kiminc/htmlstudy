var express = require('express'),
		app = express(),
        http = require('http').createServer(app),
        path = require('path'),
		bodyParser = require('body-parser'),
        _ = require('lodash');

       

        global.SERVER_ENV = app.get('env') || 'development';
        global.CONFIG   = require('./config')[SERVER_ENV];

        
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');



app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended : false })) ;
app.use(express.static(__dirname + '/public'));



app.use('/',require('./routes/index'));

http.listen(8801, function() {
	console.log('server listenling on port ' + http.address().port + ' start') ;
});
